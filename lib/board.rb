class Board

  attr_accessor :grid

  def initialize(*grid)
    if grid.empty?
      @grid = Array.new(3) { Array.new(3) }
    else
      @grid = grid[0]
    end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_marks(arr, mark)
    arr.each do |el|
      place_mark(el, mark)
    end
  end

  def grid
    return @grid
  end

  def empty?(pos)
    row, col = pos
    @grid[row][col] == nil
  end

  def positions(sym)
    arr = []
    @grid.each_with_index do |row,y|
      row.each_with_index do |cell,x|
        if cell == sym
          arr.push([y,x])
        end
      end
    end
    return arr
  end

  def is_winner?(sym)
    arr = self.positions(sym)
    (0..2).each do |n|
      if arr.include?([n,0]) && arr.include?([n,1]) && arr.include?([n,2])
        return true
      end
    end
    (0..2).each do |n|
      if arr.include?([0,n]) && arr.include?([1,n]) && arr.include?([2,n])
        return true
      end
    end
    if arr.include?([0,0]) && arr.include?([1,1]) && arr.include?([2,2])
      return true
    end
    if arr.include?([0,2]) && arr.include?([1,1]) && arr.include?([2,0])
      return true
    end
    return false
  end

  def winner
    if self.is_winner?(:X)
      return :X
    elsif self.is_winner?(:O)
      return :O
    else
      return nil
    end
  end

  def over?
    if self.is_winner?(:X) || self.is_winner?(:O) || self.positions(nil) == []
      return true
    else
      return false
    end
  end



end
