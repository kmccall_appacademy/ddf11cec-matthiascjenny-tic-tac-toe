class ComputerPlayer

  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
    @board = []
    @mark = :O
  end

  def display(brd)
    @board = brd
  end

  def winning_move
    nil_positions = @board.positions(nil)
    nil_positions.each do |pos|
      @board.place_mark(pos, @mark)
      if @board.is_winner?(@mark)
        @board.place_mark(pos, nil)
        return pos
      else
        @board.place_mark(pos, nil)
      end
    end
    return nil
  end



  def get_move
    nil_positions = @board.positions(nil)
    if self.winning_move != nil
      return self.winning_move
    else
      random_empty = Random.rand(0..nil_positions.length-1)
      return nil_positions[random_empty]
    end
  end
end
