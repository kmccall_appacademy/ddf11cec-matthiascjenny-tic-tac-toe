class HumanPlayer

  attr_accessor :name, :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def get_move
    puts "Where do you want to place your mark?"
    input = gets.chomp
    return [input[0].to_i,input[-1].to_i]
  end

  def display(board)
    puts board.grid
  end
end
